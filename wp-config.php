<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '1234' );

/** Database hostname */
define( 'DB_HOST', '127.0.0.1:3307' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Zi3a7#%|aU4W0r-sy$a^/E&s:5g@texooN` `Y[5)4~ P> R7@#pcvSpBo(WH>69' );
define( 'SECURE_AUTH_KEY',  'U v?!IGfuJPTs+H7@=NqGy6va`JN)5<gz~D=LJ>;]=sZ rD*DO;X3Y(f.Wi(pGet' );
define( 'LOGGED_IN_KEY',    '_`9qN,u$kIp -q|G@iWJgh(]4jF(K{/Aaq-b#@p*LFD_OE4k6l1O|GX^Hc{0d[RH' );
define( 'NONCE_KEY',        'G)%_Etnd_Ysdhj,Ostz0vcUYrP$g:o@U <wPcP266bO`LV`m3!/##Ioa88`{Nn?h' );
define( 'AUTH_SALT',        '!6Kl0uH/sD<Em|)O2;,M_Yv,v(nVok_SA]^9?_cYYP(H.E_~Wg<~L_OU~A3=Y5.Z' );
define( 'SECURE_AUTH_SALT', 'gBJaUG+`L(E@]_=#*dkn{wrT$KU:4L:+:JwEWRLmQ~>~{g|&+ri>s*sYV%5OS2P3' );
define( 'LOGGED_IN_SALT',   '6x(1l14ott~C]n[ 0kHi?G[Y6us`&0wcTfls{7U#?C_t5VwzXwr^&(7(p8Z1R{@T' );
define( 'NONCE_SALT',       ')H`adI+X0!0x5@]Z3HV%odx(Ui+u.;ITK&5iQPT /X%%2I+P[Oeg4DlXBrKTX[M?' );

define('JWT_AUTH_SECRET_KEY', 'secret');
define('JWT_AUTH_CORS_ENABLE', true);

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
