<?php 
    $meta = get_post_meta( $post->ID );
    $link_text = get_post_meta( $post->ID, 'mv_slider_link_text', true );
    $link_url = get_post_meta( $post->ID, 'mv_slider_link_url', true );
    $link_thumb = get_post_meta( $post->ID, 'mv_slider_link_thumb', true );
    //$attachment_id = get_post_meta( $attachment->ID, 'mv_slider_link_test', true );
    
?>
<table class="form-table mv-slider-metabox"> 
<input type="hidden" name="mv_slider_nonce" value="<?php echo wp_create_nonce( "mv_slider_nonce" ); ?>">
    <tr>
        <th>
            <label for="mv_slider_link_text">Link Text</label>
        </th>
        <td>
            <input 
                type="text" 
                name="mv_slider_link_text" 
                id="mv_slider_link_text" 
                class="regular-text link-text"
                value="<?php echo ( isset( $link_text ) ) ? esc_html( $link_text ) : ''; ?>"
                
            >
        </td>
    </tr>
    <tr>
        <th>
            <label for="mv_slider_link_url">Link URL</label>
        </th>
        <td>
            <input 
                type="url" 
                name="mv_slider_link_url" 
                id="mv_slider_link_url" 
                class="regular-text link-url"
                value="<?php echo ( isset( $link_url ) ) ? esc_url( $link_url ) : ''; ?>"
               
            >
        </td>
    </tr>    
    <tr>
        <th>
            <label for="mv_slider_link_thumb">Thumbnail</label>
        </th>
        <td>
            <input 
                type="file" 
                name="mv_slider_link_thumb" 
                id="mv_slider_link_thumb" 
                accept="image/*"
                class="regular-text link-thumb"
                value="<?php echo ( isset( $link_thumb ) ) ? esc_attr( $link_thumb ) : ''; ?>"
                
            >
        </td>
    </tr>               
</table>