import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'



export class PostItem extends Component {
    state = {
        imageUrl : '',
        isLoaded : false
    }
    static propTypes = {
        post: PropTypes.object.isRequired
    }

    componentDidMount() {
        const { featured_media} = this.props.post
        const getImageUrl = axios.get(`/wp-json/wp/v2/media/${featured_media}`).then(
            res => 
            this.setState({
                
                imageUrl: res.data.media_details.sizes.full.source_url,
                isLoaded: true
            })
           
        ).catch(err => console.log(err))
    }
  render() {
      const { title } = this.props.post
      const { imageUrl, isLoaded } = this.state
      console.log(this.state.imageUrl)
      if(isLoaded){
        
      const clickHandler = (e) => {
        e.preventDefault();
        axios.get(`/wp-json/wp/v2/media/52`).then(
            res => 
            console.log('res is', res)
      ).catch(err => console.log(err))
        }
    return (
      <div>
          <h2>{title.rendered}</h2>
          <img style={{width:"200px", height:"150px" }} src={imageUrl} alt="" />
          <button onClick={(e)=> clickHandler(e)}></button>

      </div>
    )
      } return (
          <div>Loading...</div>
      )
  }
}

export default PostItem
